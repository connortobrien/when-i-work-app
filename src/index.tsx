import * as React from 'react'
import { render } from 'react-dom'
import "react-datepicker/dist/react-datepicker.css"

import App from './app'

render(
	<App />,
	document.getElementById('root'),
)
