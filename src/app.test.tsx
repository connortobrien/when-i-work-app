import * as React from 'react'
import { render, cleanup } from '@testing-library/react'
import 'jest-dom/extend-expect'

import App from './app'

afterEach(cleanup)

test('Component renders', async () => {
	const { getByText } = render(<App />)
	expect(getByText('Welcome to the app!')).toBeInTheDocument()
})
