import * as React from 'react'

import Shifts from './Shifts'

const App = () => (
	<div className="when-i-work-applicaton">
		<Shifts />
	</div>
)

export default App
