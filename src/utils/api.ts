import { IShift, IShiftJSON, IShiftPatch } from '../Shifts/types'
import { IUser, IUserJSON } from '../Users/types'

const baseURL = 'http://localhost:8080';

export const fetchShifts = async () => {
	const data = await (await fetch(`${baseURL}/api/shifts/`)).json()
	return data.map((shift: IShiftJSON) => mapShift(shift))
}

export const saveShift = async (shift: IShiftPatch) => await (await fetch(`${baseURL}/api/shifts/`, { method: 'POST', body: JSON.stringify(shift) })).json()

export const deleteShift = async (id: number) => await fetch(`${baseURL}/api/shifts/${id}`, { method: 'DELETE' })

export const patchShift = async (id: number, shift: IShiftPatch) => await (await fetch(`${baseURL}/api/shifts/${id}`, { method: 'PATCH', body: JSON.stringify(shift) })).json()

export const fetchUsers = async () => {
	const data = await (await fetch(`${baseURL}/api/users/`)).json()
	return data.map((user: IUserJSON) => mapUser(user))
}

const mapShift = (shiftJSON: IShiftJSON): IShift => ({
	...shiftJSON,
	start: new Date(shiftJSON.start),
	end: new Date(shiftJSON.end),
	createdAt: new Date(shiftJSON.createdAt),
	updatedAt: new Date(shiftJSON.updatedAt),
})

const mapUser = (userJSON: IUserJSON): IUser => ({
	...userJSON,
	createdAt: new Date(userJSON.createdAt),
	updatedAt: new Date(userJSON.updatedAt),
})
