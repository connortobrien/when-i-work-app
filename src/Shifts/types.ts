import { IUser } from '../Users/types'

export interface IShiftPatch {
	userId?: number
	start: Date
	end: Date
}

export interface IShiftJSON {
	id: number
	user: IUser
	start: string
	end: string
	createdAt: string
	updatedAt: string
}

export interface IShift extends IShiftPatch {
	id: number
	user: IUser
	createdAt: Date
	updatedAt: Date
}
