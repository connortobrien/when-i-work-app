import * as React from 'react'

import { patchShift, deleteShift } from '../utils/api'
import { IShift, IShiftPatch } from './types'
import ShiftView from './ShiftView'
import ShiftEdit from './ShiftEdit'

const { useState } = React

interface IProps extends IShift {
	editShift: (index: number, shift: IShiftPatch) => void
	removeShift: (index: number) => void
}

const Shift = (props: IProps) => {
	const { id, removeShift, editShift, start, end, user } = props
	const [isEditable, setEditable] = useState(false)
	const remove = async () => {
		await deleteShift(id)
		removeShift(id)
	}
	const edit = async (shift: IShiftPatch) => {
		const editedShift = await patchShift(id, shift)
		editShift(id, editedShift)
		setEditable(false)
	}

	return (
		<div className="shift-view-container">
			{isEditable ? (
				<ShiftEdit
					save={edit}
					start={start}
					end={end}
					userEmail={user.email}
				/>
			) : (
				<ShiftView
					remove={remove}
					enableEdit={() => setEditable(true)}
					start={start}
					end={end}
					userEmail={user.email}
				/>
			)}
		</div>
	)
}

export default Shift
