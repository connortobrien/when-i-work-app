import * as React from 'react'
import DatePicker from 'react-datepicker'

import { IShiftPatch } from './types'
import UserSelect from '../Users/UserSelect'

const { useState } = React

interface IProps {
	save: (shift: IShiftPatch) => void
	start: Date
	end: Date
	userEmail: string
	canEditEmail?: boolean
}

const ShiftEdit = ({ save, userEmail, start, end, canEditEmail = false }: IProps) => {
	const [userId, setUserId] = useState(null)
	const [startDate, setStartDate] = useState(start)
	const [endDate, setEndDate] = useState(end)
	const onSave = () => {
		const body: IShiftPatch = {
			start: startDate,
			end: endDate,
		}
		if (canEditEmail) { body.userId = userId }
		save(body)
	}

	return (
		<div className="shift-view-edit">
			{canEditEmail ? (
				<label>
					Guest:
					<UserSelect onSelect={setUserId}/>
				</label>
			) : (
				<span><b>Email:</b> {userEmail}</span>
			)}
			<label htmlFor="start-date">
				Start:
				<DatePicker
					selected={startDate}
					onChange={(value: Date) => setStartDate(value)}
					showTimeSelect
					timeIntervals={15}
					timeFormat="hh:mm aa"
					dateFormat="MMMM d, yyyy h:mm aa"
					timeCaption="Time"
				/>
			</label>
				End:
			<label htmlFor="end-date">
				<DatePicker
					selected={endDate}
					onChange={(value: Date) => setEndDate(value)}
					showTimeSelect
					timeIntervals={15}
					timeFormat="hh:mm aa"
					dateFormat="MMMM d, yyyy h:mm aa"
					timeCaption="Time"
				/>
			</label>
			<button type="button" onClick={onSave}>Save</button>
		</div>
	)
}

export default ShiftEdit
