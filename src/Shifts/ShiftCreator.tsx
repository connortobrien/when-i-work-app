import * as React from 'react'

import { saveShift } from '../utils/api'
import { IShift, IShiftPatch } from './types'
import ShiftEdit from './ShiftEdit'

const { useState } = React

interface IProps {
	createShift: (shift: IShift) => void
}

const ShiftCreator = ({ createShift }: IProps) => {
	const [isCreating, setCreating] = useState(false)
	const saveNewShift = async (shift: IShiftPatch) => {
		const newShift = await saveShift(shift)
		console.log(newShift)
		createShift(newShift)
	}

	return (
		<div>
			{isCreating ? (
				<div className="shift-view-container">
					<ShiftEdit
						start={null}
						end={null}
						userEmail=""
						save={saveNewShift}
						canEditEmail
					/>
				</div>
			) : (
				<button type="button" onClick={() => setCreating(true)}>+ Add Shift</button>
			)}
		</div>
	)
}

export default ShiftCreator
