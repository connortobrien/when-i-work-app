import * as React from 'react'

import { IShift } from './types'

interface IProps {
	remove: () => void
	enableEdit: () => void
	start: Date
	end: Date
	userEmail: string
}

const ShiftView = ({ remove, enableEdit, start, end, userEmail }: IProps) => {
	return (
		<ul className="shift-view">
			<li><b>Email:</b> {userEmail}</li>
			<li><b>Start:</b> {start.toString()}</li>
			<li><b>End:</b> {end.toString()}</li>
			<li className="button-group">
				<button type="button" onClick={enableEdit}>Edit</button>
				<button type="button" onClick={remove}>Delete</button>
			</li>
		</ul>
	)
}

export default ShiftView
