import * as React from 'react'

import { IShift } from './types'
import { fetchShifts } from '../utils/api'
import Loading from '../Loading'
import ShiftList from './ShiftList'
import ShiftCreator from './ShiftCreator'
import './shifts.css'

const { useState, useEffect } = React

const Shifts = () => {
	const [shifts, setShifts] = useState([])
	const [loading, setLoading] = useState(false)
	const createShift = (shift: IShift) => setShifts([ ...shifts, shift ])
	const editShift = (id: number, shift: IShift) => setShifts(shifts.map((s) => s.id === id ? shift : s))
	const removeShift = (id: number) => {
		console.log('remove hit', id)
		return setShifts(shifts.filter((s) => s.id !== id))
	}

	useEffect(() => {
		setLoading(true)
		const fetchData = async () => {
			setShifts(await fetchShifts())
			setLoading(false)
		}
		fetchData()
	}, [])

	return (
		<div className="shift-container">
			{!loading && (
				<ShiftList
					shifts={shifts}
					removeShift={removeShift}
					editShift={editShift}
				/>
			)}
			{loading && <Loading />}
			<ShiftCreator createShift={createShift} />
		</div>
	)
}

export default Shifts
