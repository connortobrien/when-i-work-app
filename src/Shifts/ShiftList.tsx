import * as React from 'react'

import { IShift } from './types'
import Shift from './Shift'

interface IProps {
	shifts: IShift[]
	editShift: (index: number, shift: IShift) => void
	removeShift: (index: number) => void
}

const ShiftList = ({ shifts = [], editShift, removeShift }: IProps) => {
	return (
		<div className="shift-list">
			{shifts.map(shift => <Shift key={shift.id} {...shift} editShift={editShift} removeShift={removeShift} />)}
		</div>
	)
}

export default ShiftList
