interface BaseUser {
	id: number
	email: string
	firstName: string
	lastName: string
}

export interface IUser extends BaseUser {
	createdAt: Date
	updatedAt: Date
}

export interface IUserJSON extends BaseUser {
	createdAt: string
	updatedAt: string
}
