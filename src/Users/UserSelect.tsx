import * as React from 'react'

import { fetchUsers } from '../utils/api'

const { useState, useEffect } = React

interface IProps {
	onSelect: (id: number) => void
}

const UserSelect = ({ onSelect }: IProps) => {
	const [users, setUsers] = useState([])
	const [loading, setLoading] = useState(false)

	useEffect(() => {
		setLoading(true)
		const fetchData = async () => {
			setUsers(await fetchUsers())
			setLoading(false)
		}
		fetchData()
	}, [])

	return (
		<select onChange={event => onSelect(parseInt(event.target.value))}>
			<option />
			{users.map(user => (
				<option
					key={user.id}
					value={user.id}
				>
					{`${user.firstName} ${user.lastName}`}
				</option>
			))}
		</select>
	)
}

export default UserSelect
