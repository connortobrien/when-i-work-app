const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpack = require('webpack');

module.exports = {
	mode: 'development',
	entry: path.resolve(__dirname, 'src/index.tsx'),
	devtool: 'inline-source-map',
	output: {
		filename: '[name].bundle.js',
		path: path.resolve(__dirname, 'dist'),
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			},
			{
				test: /\.css$/,
				use: ['style-loader', 'css-loader'],
			},
		],
	},
	resolve: {
		extensions: ['.js', '.ts', '.tsx'],
	},
	devServer: {
		port: 3000,
		contentBase: path.join(__dirname, 'dist'),
		// historyApiFallback: true,
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: 'When I Work',
			template: path.resolve(__dirname, 'public/index.html'),
		}),
		new webpack.HotModuleReplacementPlugin(),
	],
};
